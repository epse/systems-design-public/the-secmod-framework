# The SecMOD Framework

The SecMOD Framework, an Open-Source Modular Framework Combining Multi-Sector System Optimization and Life-Cycle Assessment, has been developed by members of the EPSE group in the following paper:

> [Reinert, C., Schellhas, L., Mannhardt, J., Shu, D., Kämper, A., Baumgärtner, N., Deutz, S., & Bardow, A. (2022). SecMOD: An Open-Source Modular Framework Combining Multi-Sector System Optimization and Life-Cycle Assessment. Frontiers in Energy Research, 10, 884525](https://doi.org/10.3389/fenrg.2022.884525)

You can find the corresponding Git here: 

https://git-ce.rwth-aachen.de/ltt/secmod
